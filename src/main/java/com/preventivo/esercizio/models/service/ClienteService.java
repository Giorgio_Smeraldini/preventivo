package com.preventivo.esercizio.models.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.preventivo.esercizio.models.Cliente;

@Service
public class ClienteService {

	@Autowired
	private EntityManager entMan;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class);
	}
	
	public Cliente saveCli (Cliente objCli) {
		
		Cliente temp = new Cliente();
	 	temp.setIntestatario_azienda(objCli.getIntestatario_azienda());
	 	temp.setNome_cliente(objCli.getNome_cliente());
	 	temp.setVia(objCli.getVia());
	 	
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return temp;
 	}
	
	public List<Cliente> findAll(){
		Session sessione = getSessione();
		return sessione.createCriteria(Cliente.class).list();
	}
	
	@Transactional
	public boolean delete (int varId) {
		
		Session sessione = getSessione();
		
		try {
			Cliente temp = sessione.load(Cliente.class, varId);
			sessione.delete(temp);
			sessione.flush();
			return true;
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
		
	}
	
	@Transactional
	public boolean update (Cliente objCliente) {
		
		Session sessione = getSessione();
//		System.out.println(objCliente.getTitolo());
		try {
			Cliente temp = sessione.load(Cliente.class, objCliente.getId());
			
			if(temp!=null)
			{
				temp.setNome_cliente(objCliente.getNome_cliente());
				temp.setIntestatario_azienda(objCliente.getIntestatario_azienda());
				temp.setVia(objCliente.getVia());
			
				sessione.update(temp);
				sessione.flush();
				
				return true;

			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	
		return false;
		
		
		
	}
	 
	
}// end public class ClienteService 
