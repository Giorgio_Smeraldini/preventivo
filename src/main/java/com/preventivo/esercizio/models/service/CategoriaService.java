package com.preventivo.esercizio.models.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.preventivo.esercizio.models.Categoria;


@Service
public class CategoriaService {

	@Autowired
	private EntityManager entMan;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class);
	}
	
	public Categoria saveCat (Categoria objCat) {
		
		Categoria temp = new Categoria();
		temp.setNome_categoria(objCat.getNome_categoria());
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return temp;
 	}
	
	public List<Categoria> findAll(){
		Session sessione = getSessione();
		return sessione.createCriteria(Categoria.class).list();
	}
	

}
