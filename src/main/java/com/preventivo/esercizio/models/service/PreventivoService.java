package com.preventivo.esercizio.models.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.preventivo.esercizio.models.Preventivo;

@Service
public class PreventivoService {

	@Autowired
	private EntityManager entMan;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class);
	}
	
	public Preventivo savePrev (Preventivo objPrev) {
		
		Preventivo temp = new Preventivo();
		temp.setCliente(objPrev.getCliente());
		
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return temp;
 	}
	
	public List<Preventivo> findAll(){
		Session sessione = getSessione();
		return sessione.createCriteria(Preventivo.class).list();
	}
	
}
