package com.preventivo.esercizio.models.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.preventivo.esercizio.models.Preventivo;
import com.preventivo.esercizio.models.Prodotto;

@Service
public class ProdottoService {

	@Autowired
	private EntityManager entMan;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class);
	}
	
	public Prodotto savePrev (Prodotto objProd) {
		
		Prodotto temp = new Prodotto();
		temp.setCodice_prodotto(objProd.getCodice_prodotto());
		temp.setNome_prodotto(objProd.getNome_prodotto());
		temp.setPrezzo_prodotto(objProd.getPrezzo_prodotto());
		temp.setCategoria_rif(objProd.getCategoria_rif());
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return temp;
 	}
	
	public List<Prodotto> findAll(){
		Session sessione = getSessione();
		return sessione.createCriteria(Prodotto.class).list();
	}
	
}
