package com.preventivo.esercizio.models;



import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Preventivo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int preventivo_id;
	
	
	@Column
	String data_preventivo;
	
	@ManyToOne
	@JoinColumn(name="cliente_rif")
	private Cliente cliente;
	
	@ManyToMany
	@JoinTable(name="appoggio_preventivo_prodotto",
				joinColumns = {@JoinColumn(name="preventivo_rif", referencedColumnName= "preventivo_id")},
				inverseJoinColumns = {@JoinColumn(name="prodotto_rif", referencedColumnName = "prodotto_id")})
	
	private List<Prodotto> prodotti;
	
	

	public Preventivo() {
		
	}
	
	public Preventivo(int preventivo_id, String data_preventivo, Cliente cliente, List<Prodotto> prodotti) {
		super();
		this.preventivo_id = preventivo_id;
		this.data_preventivo = data_preventivo;
		this.cliente = cliente;
		this.prodotti = prodotti;
	}

	public int getPreventivo_id() {
		return preventivo_id;
	}

	public void setPreventivo_id(int preventivo_id) {
		this.preventivo_id = preventivo_id;
	}

	public String getData_preventivo() {
		return data_preventivo;
	}

	public void setData_preventivo(String data_preventivo) {
		this.data_preventivo = data_preventivo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Prodotto> getProdotti() {
		return prodotti;
	}

	public void setProdotti(List<Prodotto> prodotti) {
		this.prodotti = prodotti;
	}
	
	
	
	
	
	
	
}
