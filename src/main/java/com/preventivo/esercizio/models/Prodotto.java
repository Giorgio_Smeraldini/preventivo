package com.preventivo.esercizio.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table
public class Prodotto {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int prodotto_id;
	
	@Column
	private String nome_prodotto;
	@Column
	private String codice_prodotto;
	@Column
	private int quantita_prodotto;
	@Column
	private float prezzo_prodotto;
	@Column
	private int categoria_rif;
	
	
	@ManyToMany
	@JoinTable(name="appoggio_prodotto_categoria",
				joinColumns = {@JoinColumn(name="codice_prodotto_rif", referencedColumnName= "prodotto_id")},
				inverseJoinColumns = {@JoinColumn(name="codice_categoria_rif", referencedColumnName = "categoria_id")})
	
	private List<Categoria> categorie;
	

	@ManyToMany
	@JoinTable(name="appoggio_preventivo_prodotto",
				joinColumns = {@JoinColumn(name="prodotto_rif", referencedColumnName= "prodotto_id")},
				inverseJoinColumns = {@JoinColumn(name="preventivo_rif", referencedColumnName = "preventivo_id")})
	
	private List<Preventivo> preventivi;

	
	public Prodotto() {
		
	}

	public Prodotto(int prodotto_id, String nome_prodotto, String codice_prodotto, int quantita_prodotto,
			float prezzo_prodotto, int categoria_rif, List<Categoria> categorie, List<Preventivo> preventivi) {
		super();
		this.prodotto_id = prodotto_id;
		this.nome_prodotto = nome_prodotto;
		this.codice_prodotto = codice_prodotto;
		this.quantita_prodotto = quantita_prodotto;
		this.prezzo_prodotto = prezzo_prodotto;
		this.categoria_rif = categoria_rif;
		this.categorie = categorie;
		this.preventivi = preventivi;
	}


	public int getProdotto_id() {
		return prodotto_id;
	}


	public void setProdotto_id(int prodotto_id) {
		this.prodotto_id = prodotto_id;
	}


	public String getNome_prodotto() {
		return nome_prodotto;
	}


	public void setNome_prodotto(String nome_prodotto) {
		this.nome_prodotto = nome_prodotto;
	}


	public String getCodice_prodotto() {
		return codice_prodotto;
	}


	public void setCodice_prodotto(String codice_prodotto) {
		this.codice_prodotto = codice_prodotto;
	}


	public int getQuantita_prodotto() {
		return quantita_prodotto;
	}


	public void setQuantita_prodotto(int quantita_prodotto) {
		this.quantita_prodotto = quantita_prodotto;
	}


	public float getPrezzo_prodotto() {
		return prezzo_prodotto;
	}


	public void setPrezzo_prodotto(float prezzo_prodotto) {
		this.prezzo_prodotto = prezzo_prodotto;
	}


	public int getCategoria_rif() {
		return categoria_rif;
	}


	public void setCategoria_rif(int categoria_rif) {
		this.categoria_rif = categoria_rif;
	}
	
	
	
	
}
