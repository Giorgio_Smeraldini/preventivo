package com.preventivo.esercizio.models.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.preventivo.esercizio.models.Categoria;
import com.preventivo.esercizio.models.service.CategoriaService;



@RestController
@RequestMapping("/categorie")
@CrossOrigin(origins="*", allowedHeaders="*")
public class CategoriaController {
	
	@Autowired
	private CategoriaService service;
	
	@GetMapping("/test")
	public String test() {
		return "DAI CATEGORIA";
	}
	
	@PostMapping("/nuovaCategoria")
	public Categoria saveCat (@RequestBody Categoria objCat) {
		return service.saveCat(objCat);
	}
	
	@GetMapping("/")
	public List<Categoria> findAll(){
		return service.findAll();
	}
	

}
