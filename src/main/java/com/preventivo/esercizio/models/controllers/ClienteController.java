package com.preventivo.esercizio.models.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.preventivo.esercizio.models.Cliente;
import com.preventivo.esercizio.models.service.ClienteService;


@RestController
@RequestMapping("/clienti")
@CrossOrigin(origins="*", allowedHeaders="*")
public class ClienteController {
	
	@Autowired
	private ClienteService service;
	
	@GetMapping("/test")
	public String test() {
		return "DAI";
	}
	
	@PostMapping("/nuovoCliente")
	public Cliente saveCli (@RequestBody Cliente objCli) {
		return service.saveCli(objCli);
	}
	
	@GetMapping("/")
	public List<Cliente> findAll(){
		return service.findAll();
	}
	
	@DeleteMapping ("/{id_delete}")
	public boolean eliminaCliente(@PathVariable int id_delete) {
		return service.delete(id_delete);
	}
	
	@PutMapping("/update")
	public boolean modificaCliente (@RequestBody Cliente objLibro) {
		return service.update(objLibro);
	}
	
	
}
