package com.preventivo.esercizio.models.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.preventivo.esercizio.models.Prodotto;

import com.preventivo.esercizio.models.service.ProdottoService;

@RestController
@RequestMapping("/prodotti")
@CrossOrigin(origins="*", allowedHeaders="*")
public class ProdottoController {


	@Autowired
	private ProdottoService service;
	
	@GetMapping("/test")
	public String test() {
		return "DAI Prodotto";
	}
	
	@PostMapping("/nuovoProdotto")
	public Prodotto savePrev (@RequestBody Prodotto objCli) {
		return service.savePrev(objCli);
	}
	
	@GetMapping("/")
	public List<Prodotto> findAll(){
		return service.findAll();
	}
	
	
}
