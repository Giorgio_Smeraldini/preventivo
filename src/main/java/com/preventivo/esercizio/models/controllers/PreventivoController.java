package com.preventivo.esercizio.models.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.preventivo.esercizio.models.Preventivo;
import com.preventivo.esercizio.models.service.PreventivoService;

@RestController
@RequestMapping("/preventivi")
@CrossOrigin(origins="*", allowedHeaders="*")
public class PreventivoController {

	@Autowired
	private PreventivoService service;
	
	@GetMapping("/test")
	public String test() {
		return "DAI Preventivo";
	}
	
	@PostMapping("/nuovoPreventivo")
	public Preventivo savePrev (@RequestBody Preventivo objPrev) {
		return service.savePrev(objPrev);
	}
	
	@GetMapping("/")
	public List<Preventivo> findAll(){
		return service.findAll();
	}
	
}
