package com.preventivo.esercizio.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Azienda {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="azienda_id")
	private int azienda_id;
	
	@Column
	String nome;
	@Column
	String intestatario;
	@Column
	String via;
	
	public Azienda () {
		
	}
	
	public Azienda(int azienda_id, String nome, String intestatario, String via) {
		super();
		this.azienda_id = azienda_id;
		this.nome = nome;
		this.intestatario = intestatario;
		this.via = via;
	}
	public int getAzienda_id() {
		return azienda_id;
	}
	public void setAzienda_id(int azienda_id) {
		this.azienda_id = azienda_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getIntestatario() {
		return intestatario;
	}
	public void setIntestatario(String intestatario) {
		this.intestatario = intestatario;
	}
	public String getVia() {
		return via;
	}
	public void setVia(String via) {
		this.via = via;
	}
	
	
	
}
