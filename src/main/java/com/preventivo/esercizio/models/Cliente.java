package com.preventivo.esercizio.models;

import java.util.List;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Cliente {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="cliente_id")
	private int id;
	
	@Column
	private String nome_cliente;
	@Column
	private String intestatario_azienda;
	@Column
	private String via;
	
	@OneToMany(mappedBy = "cliente")
	private List<Preventivo> elenco_preventivi;
	
	public Cliente() {
		
	};
	
	public Cliente(int id, String nome_cliente, String intestatario_azienda, String via) {
		super();
		this.id = id;
		this.nome_cliente = nome_cliente;
		this.intestatario_azienda = intestatario_azienda;
		this.via = via;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome_cliente() {
		return nome_cliente;
	}
	public void setNome_cliente(String nome_cliente) {
		this.nome_cliente = nome_cliente;
	}
	public String getIntestatario_azienda() {
		return intestatario_azienda;
	}
	public void setIntestatario_azienda(String intestatario_azienda) {
		this.intestatario_azienda = intestatario_azienda;
	}
	public String getVia() {
		return via;
	}
	public void setVia(String via) {
		this.via = via;
	}
	
	
	
}
