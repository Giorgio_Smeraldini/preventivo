package com.preventivo.esercizio.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table
public class Categoria {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int categoria_id;
	
	@Column
	private String nome_categoria;
	
	@ManyToMany
	@JoinTable(name="appoggio_prodotto_categoria",
				joinColumns = {@JoinColumn(name="codice_categoria_rif", referencedColumnName= "categoria_id")},
				inverseJoinColumns = {@JoinColumn(name="codice_prodotto_rif", referencedColumnName = "prodotto_id")})
	
	
	private List<Prodotto> prodotti;
	
	public Categoria() {
		
	}

	public Categoria(int categoria_id, String nome_categoria) {
		super();
		this.categoria_id = categoria_id;
		this.nome_categoria = nome_categoria;
	}

	public int getCategoria_id() {
		return categoria_id;
	}

	public void setCategoria_id(int categoria_id) {
		this.categoria_id = categoria_id;
	}

	public String getNome_categoria() {
		return nome_categoria;
	}

	public void setNome_categoria(String nome_categoria) {
		this.nome_categoria = nome_categoria;
	}

	
	
}
